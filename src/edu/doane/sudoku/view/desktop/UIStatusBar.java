package edu.doane.sudoku.view.desktop;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Status bar for the desktop SuDoKu game.
 *
 * @author Mark M. Meysenburg
 * @version 12/16/2015
 */
public class UIStatusBar extends JPanel {
	/**
	 * Silence the Eclipse warning about serialization, even though
	 * we're not doing that. 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Label displaying the elapsed time.
     */
    private final JLabel lblTimer;

    /**
     * Label displaying notes mode status.
     */
    private final JLabel lblNotesMode;
    
    /**
     * Label displaying pause mode status.
     */
    private final JLabel lblPauseMode;

    /**
     * Label displaying assist mode status.
     */
    private final JLabel lblAssistMode;
    
    /**
     * Construct the status bar.
     */
    public UIStatusBar() {
        super();

        setLayout(new GridLayout(1, 2));

        lblTimer = new JLabel("0:00:00");

        JPanel pnlTimer = new JPanel();
        pnlTimer.setLayout(new FlowLayout());
        pnlTimer.add(lblTimer);
        add(pnlTimer);

        lblNotesMode = new JLabel();
        setNormalMode();
        JPanel pnlNotesMode = new JPanel();
        pnlNotesMode.setLayout(new FlowLayout());
        pnlNotesMode.add(lblNotesMode);
        add(pnlNotesMode);
        
        lblPauseMode = new JLabel();
        setResumeMode();
        JPanel pnlPauseMode = new JPanel();
        pnlPauseMode.setLayout(new FlowLayout());
        pnlPauseMode.add(lblPauseMode);
        add(pnlPauseMode);
        
        lblAssistMode = new JLabel();
        setDisableAssist();
        JPanel pnlAssistMode = new JPanel();
        pnlAssistMode.setLayout(new FlowLayout());
        pnlAssistMode.add(lblAssistMode);
        add(pnlAssistMode);
    }

    /**
     * Toggle notes mode on.
     */
    public final void setNotesMode() {
        lblNotesMode.setText("(N)otes mode: on");
    }

    /**
     * Toggle notes mode off.
     */
    public final void setNormalMode() {
        lblNotesMode.setText("(N)otes mode: off");
    }
    
    /**
     * Toggle pause mode on.
     */
    public final void setPauseMode() {
        lblPauseMode.setText("(P)ause mode: on");
    }
    
    /**
     * Toggle pause mode off.
     */
    public final void setResumeMode() {
        lblPauseMode.setText("(P)ause mode: off");
    }
    
    /**
     * Toggle assist mode on.
     */
    public final void setEnableAssist(){
        lblAssistMode.setText("(A)ssist mode: on");
    }
    
    /**
     * Toggle assist mode off.
     */
    public final void setDisableAssist(){
        lblAssistMode.setText("(A)ssist mode: off");
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
    
    /**
     * Set the time value displayed on the status bar.
     * 
     * @param time Time value to set, in 0:00:00 format.
     */
    public void setTime(String time) {
        lblTimer.setText(time);
    }
}
